<?php
/**
 * @author Daniel Torac
 */
class PigLatin{
    /**
     * constants title, original, translated & footer
     */
    const TITLE      = "<h1> Pig Latin translator </h1><hr />";
    const ORIGINAL   = "<h3> Original text: </h3>";
    const TRANSLATED = "<h3> Translated text: </h3>";
    const FOOTER     = "<hr /><small>&copy; 2018 Daniel Torac</small>";

    // public metoda vrati obsah z .txt suboru
    public function originalContent(){
        return file_get_contents( 'content.txt', FILE_USE_INCLUDE_PATH );
    }

    private $text; // private definicia premennej $text
    // public metoda spracuje a vrati slovo podla vzoroveho zadania pomocou REGEX
    public function getTranslate( $text ){
        $original   = preg_replace( "#^([^aeio]+)([aeiou]+)(.*)#", "$2$3-$1ay", $this->text = $text );    
        $translated = preg_replace( "#(^[aeiou].*)#", "$1", $original );
        return $translated;
    }        
}

// vypis titulku
echo PigLatin::TITLE, PHP_EOL;

// vypis titulku original text
echo PigLatin::ORIGINAL, PHP_EOL;

$original = new PigLatin();         // instancia objektu original
echo $original->originalContent();  // vypis originalneho textu

echo PigLatin::TRANSLATED, PHP_EOL; // vypis titulku translated text
// rozdelenie stringu na jednotlicve stringy (znaky) a vrati pole
$content = explode( ' ', $original->originalContent() );

// nahradenie bielym znakom bodku
$content = str_replace('.', ' ', $content );

$translated = new PigLatin();       // instancia objektu translated
// cyklus vypise pole prelozeny text
foreach($content as $key){
    echo $translated->getTranslate( $key . "\n" );
}

// vypis footer
echo PigLatin::FOOTER, PHP_EOL;
?>